import json

import numpy as np
import requests
from PIL import Image
from flask import Flask, request, Response
from flask_cors import CORS

labels = np.genfromtxt(fname="./resources/labels.txt", dtype='str')

# run api
api = Flask(__name__)
CORS(api)


@api.route("/predict", methods=["POST"])
def predict_file():
    file = request.files['file']
    # Decoding and pre-processing base64 image
    image = load_image(file, target_size=(224, 224,))
    image = image_to_array(image)
    image = preprocess_image(image)

    # Creating payload for TensorFlow serving request
    payload = {
        "instances": [image.tolist()]
    }

    # Making POST request
    r = requests.post('http://localhost:8501/v1/models/saved_model:predict', json=payload)

    # Decoding results from TensorFlow Serving server
    pred = json.loads(r.text)

    results = map_results(pred)
    response = Response(json.dumps(results))
    predictionId = strip_filename(file.filename)
    response.headers['predictionId'] = predictionId
    # Returning JSON response to the frontend
    return response, 200


def map_results(probabilities):
    result = {}
    probabilities = np.array(probabilities['predictions'][0])
    for index, probability in enumerate(probabilities):
        result[labels[index]] = float(probability)
    return result


def load_image(path, color_mode='rgb', target_size=None):
    if Image is None:
        raise ImportError('Could not import PIL.Image. '
                          'The use of `load_img` requires PIL.')
    img = Image.open(path)
    if color_mode == 'rgb':
        if img.mode != 'RGB':
            img = img.convert('RGB')
    else:
        raise ValueError('color_mode must be "rgb"')
    if target_size is not None:
        width_height_tuple = (target_size[1], target_size[0])
        if img.size != width_height_tuple:
            resample = Image.NEAREST
            img = img.resize(width_height_tuple, resample)
    return img


def image_to_array(img, data_format='channels_last', dtype='float32'):
    if data_format not in {'channels_first', 'channels_last'}:
        raise ValueError('Unknown data_format: %s' % data_format)
    # Numpy array x has format (height, width, channel)
    # or (channel, height, width)
    # but original PIL image has format (width, height, channel)
    x = np.asarray(img, dtype=dtype)
    if len(x.shape) == 3:
        if data_format == 'channels_first':
            x = x.transpose(2, 0, 1)
    elif len(x.shape) == 2:
        if data_format == 'channels_first':
            x = x.reshape((1, x.shape[0], x.shape[1]))
        else:
            x = x.reshape((x.shape[0], x.shape[1], 1))
    else:
        raise ValueError('Unsupported image shape: %s' % (x.shape,))
    return x


def preprocess_image(image):
    image /= 127.5
    image -= 1.
    return image


def strip_filename(filename):
    filename = str(filename)
    filename = filename.replace('.jpg', '')
    filename = filename.replace('.png', '')
    return filename


if __name__ == "__main__":
    api.run(port=8000, threaded=False)
